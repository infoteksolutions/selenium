# seleniumtrainingtestcases
This repository has some basic and advanced testcases of selenium webdriver. To run these testcases you need to install following tools:
1. Eclipse: download Eclipse IDE for Java developers: http://www.eclipse.org/downloads/packages/release/Neon/3 
2. Java version 7.0 or higer : http://www.oracle.com/technetwork/java/javase/downloads/index.html

Selenium testcases are dependent on some jar libraries. You need to download these libraries and add as exeternal jar files under Java project in eclipse...
1. Selenium Java libraries: https://www.seleniumhq.org/download/
2. Mysql Connector Libraries: https://dev.mysql.com/downloads/connector/j/
3. junit & hamcrest jar: https://github.com/junit-team/junit4/wiki/Download-and-Install
4. FileUtils: https://commons.apache.org/proper/commons-io/download_io.cgi

and in the end, you need to download & keep some drivers to execute test cases on browsers:
1. geckodriver for Firefox: https://github.com/mozilla/geckodriver/releases
2. chromedriver for Chrome: https://sites.google.com/a/chromium.org/chromedriver/downloads
3. IEdriver for IE edge: https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver

Please let us know if you face any issue regarding the execution of test cases... 
